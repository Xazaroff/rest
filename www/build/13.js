webpackJsonp([13],{

/***/ 485:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsPageModule", function() { return ProductDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_details__ = __webpack_require__(777);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_pipes_module__ = __webpack_require__(493);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProductDetailsPageModule = /** @class */ (function () {
    function ProductDetailsPageModule() {
    }
    ProductDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__product_details__["a" /* ProductDetailsPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__product_details__["a" /* ProductDetailsPage */]), __WEBPACK_IMPORTED_MODULE_3__app_pipes_module__["a" /* PipesModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__product_details__["a" /* ProductDetailsPage */]]
        })
    ], ProductDetailsPageModule);
    return ProductDetailsPageModule;
}());

//# sourceMappingURL=product-details.module.js.map

/***/ }),

/***/ 493:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_pipe__ = __webpack_require__(494);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(137);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__app_pipe__["a" /* AppPipe */]],
            imports: [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */], __WEBPACK_IMPORTED_MODULE_1__app_pipe__["a" /* AppPipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 494:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppPipe = /** @class */ (function () {
    function AppPipe() {
    }
    AppPipe.prototype.transform = function (value, args) {
        if (value) {
            var limit = args.length > 0 ? parseInt(args[0], 50) : 50;
            var trail = args.length > 1 ? args[1] : "...";
            return value.length > limit ? value.substring(0, limit) + trail : value;
        }
    };
    AppPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: "limitPipe"
        })
    ], AppPipe);
    return AppPipe;
}());

//# sourceMappingURL=app.pipe.js.map

/***/ }),

/***/ 777:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_cart_service__ = __webpack_require__(291);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductDetailsPage = /** @class */ (function () {
    function ProductDetailsPage(navCtrl, af, db, navParams, cartService, alertCtrl, loadingCtrl, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.af = af;
        this.db = db;
        this.navParams = navParams;
        this.cartService = cartService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.FireVisible = false;
        this.count = 1;
        this.isLiked = false;
        this.menuItems = {};
        this.cart = {
            itemId: String,
            extraOptions: [],
            price: {
                name: "",
                value: 0,
                currency: ""
            },
            title: "",
            thumb: String,
            itemQunatity: this.count
        };
        this.selectedItems = [];
        this.visible = false;
        this.currency = JSON.parse(localStorage.getItem('currency'));
        this.id = this.navParams.get("id");
        this.menuItem = db.object("/menuItems/" + this.id);
        this.menuItem.valueChanges().subscribe(function (data) {
            if (data != null) {
                _this.menuItems = data;
                _this.menuItems["$key"] = _this.id;
                _this.cart.title = data.title;
                _this.cart.itemId = _this.id;
                _this.cart.thumb = data.thumb;
            }
            if (_this.af.auth.currentUser) {
                _this.db
                    .object("/users/" + _this.af.auth.currentUser.uid + "/favourite/" + _this.id)
                    .valueChanges()
                    .subscribe(function (res) {
                    // console.log("fav response--", res);
                    if (res != null) {
                        _this.isLiked = true;
                    }
                    else {
                        _this.isLiked = false;
                    }
                });
            }
        });
    }
    ProductDetailsPage.prototype.ionViewWillEnter = function () {
        var cart = JSON.parse(localStorage.getItem("Cart"));
        this.noOfItems = cart != null ? cart.length : null;
    };
    ProductDetailsPage.prototype.addQuantity = function () {
        if (this.count < 10) {
            this.count = this.count + 1;
            this.cart.itemQunatity = this.count;
        }
    };
    ProductDetailsPage.prototype.removeQuantity = function () {
        if (this.count > 1) {
            this.count = this.count - 1;
            this.cart.itemQunatity = this.count;
        }
    };
    ProductDetailsPage.prototype.navcart = function () {
        this.navCtrl.push("CartPage");
    };
    ProductDetailsPage.prototype.home = function () {
        this.navCtrl.push("HomePage");
    };
    ProductDetailsPage.prototype.sizeOptions = function (price) {
        this.cart.price = price;
        this.cart.price.value = Number(price.value);
    };
    ProductDetailsPage.prototype.checkOptions = function (extraOption) {
        if (this.cart.extraOptions.length != 0) {
            for (var i = 0; i <= this.cart.extraOptions.length - 1; i++) {
                if (this.cart.extraOptions[i].name == extraOption.name) {
                    this.cart.extraOptions.splice(i, 1);
                    break;
                }
                else {
                    this.cart.extraOptions.push(extraOption);
                    break;
                }
            }
        }
        else {
            this.cart.extraOptions.push(extraOption);
        }
    };
    ProductDetailsPage.prototype.addToCart = function () {
        if (this.cart.price.name == "") {
            var alert_1 = this.alertCtrl.create({
                title: "Please!",
                subTitle: "Select Size and Price!",
                buttons: ["OK"]
            });
            alert_1.present();
        }
        else {
            this.cartService.OnsaveLS(this.cart);
            this.navCtrl.push("CartPage");
        }
    };
    ProductDetailsPage.prototype.addToFevrt = function (key) {
        var _this = this;
        if (this.af.auth.currentUser) {
            this.db
                .object("/users/" + this.af.auth.currentUser.uid + "/favourite/" + key)
                .update({
                thumb: this.menuItems.thumb,
                title: this.menuItems.title,
                description: this.menuItems.description
            })
                .then(function (res) {
                _this.isLiked = true;
                _this.createToaster("added to favourites", "3000");
            });
        }
        else {
            this.createToaster("please login first", "3000");
        }
    };
    ProductDetailsPage.prototype.removeFevrt = function (key) {
        var _this = this;
        if (this.af.auth.currentUser) {
            this.db
                .object("/users/" + this.af.auth.currentUser.uid + "/favourite/" + key)
                .remove()
                .then(function () {
                _this.isLiked = false;
                _this.createToaster("removed from favourites", "3000");
            });
        }
    };
    ProductDetailsPage.prototype.createToaster = function (message, duration) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration
        });
        toast.present();
    };
    ProductDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-product-details",template:/*ion-inline-start:"C:\Users\1292344\Desktop\ionic-3-restaurantapp-master\src\pages\product-details\product-details.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title class="title">{{"Restaurant App" | translate}}</ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only color="royal" (click)="navcart()" class="header-btn-cart">\n                <ion-icon name="cart"></ion-icon>\n                <ion-badge class="carts" item-right color="danger">{{noOfItems}}</ion-badge>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="item-page">\n    <ion-row class="productImage">\n        <img src={{menuItems.thumb}} alt="{{menuItems.title}} ">\n\n\n    </ion-row>\n    <!-- first-row -->\n    <!--favourite-->\n    <span clear text-center *ngIf="isLiked">\n        <ion-icon name="heart" class="fav" (click)="removeFevrt(menuItems.$key)"></ion-icon>\n    </span>\n    <span clear text-center *ngIf="!isLiked">\n        <ion-icon name="heart" class="fav heart-clicked" (click)="addToFevrt(menuItems.$key)"></ion-icon>\n    </span>\n    <ion-row>\n        <ion-col col-6>\n            <div class="count">\n                <span class="item-count" (click)="removeQuantity()">\n                    -\n                </span>\n                <span class="item-count show-count">\n                    {{count}}\n                </span>\n                <span class="item-count" (click)="addQuantity()">\n                    +\n                </span>\n            </div>\n        </ion-col>\n\n        <ion-col col-6>\n            <a (click)="addToCart()" ion-button icon-left full class="btn-add-to-cart">\n                <ion-icon name="cart"></ion-icon>\n                &nbsp; {{"ADD TO CART" | translate}}\n            </a>\n        </ion-col>\n    </ion-row>\n    <!-- row-2 -->\n    <ion-list radio-group class="size-list">\n        <ion-list-header class="size-list-header" text-center>\n            {{"Size and prices" | translate}}\n        </ion-list-header>\n        <ion-item *ngFor="let item of menuItems.price; let i= index" class="size-list-item">\n            <ion-label *ngIf="item.specialPrice"> {{item.pname}}\n                <span class="cut">{{currency?.currencySymbol}}{{item.value}}</span>\n                <span class="offer-p">{{currency?.currencySymbol}}{{item.specialPrice |number:0}}</span>\n            </ion-label>\n            <ion-label *ngIf="!item.specialPrice"> {{item.pname}} {{currency?.currencySymbol}}{{item.value}}</ion-label>\n            <ion-radio class="checked" (ionSelect)="sizeOptions(item)"></ion-radio>\n        </ion-item>\n    </ion-list>\n    <ion-list class="extra-list" *ngIf="menuItems.extraOptions != null">\n        <ion-list-header class="header" text-center>{{"Add extra choice" | translate }}</ion-list-header>\n        <ion-item *ngFor="let option of menuItems.extraOptions; let i= index" class="list-item">\n            <ion-label>{{option.name}} ({{currency?.currencySymbol}}{{option.value}})</ion-label>\n            <ion-toggle (ionChange)="checkOptions(option)"></ion-toggle>\n        </ion-item>\n    </ion-list>\n\n    <ion-row class="button-fixed">\n        <ion-col col-6>\n            <a (click)="addToCart()" ion-button icon-left full class="btn-add-to-cart">\n                <ion-icon name="cart"></ion-icon>\n                &nbsp; {{"CHECKOUT" | translate}}\n            </a>\n        </ion-col>\n        <ion-col col-6>\n            <a (click)="home()" ion-button icon-left full class="btn-add-to-cart">\n                <ion-icon name="basket"></ion-icon>\n                &nbsp; {{"KEEP SHOPPING" | translate}}\n            </a>\n        </ion-col>\n    </ion-row>\n</ion-content>'/*ion-inline-end:"C:\Users\1292344\Desktop\ionic-3-restaurantapp-master\src\pages\product-details\product-details.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__pages_cart_service__["a" /* CartService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */]])
    ], ProductDetailsPage);
    return ProductDetailsPage;
}());

//# sourceMappingURL=product-details.js.map

/***/ })

});
//# sourceMappingURL=13.js.map