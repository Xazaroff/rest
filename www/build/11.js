webpackJsonp([11],{

/***/ 489:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsModule", function() { return SettingsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings__ = __webpack_require__(781);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_pipes_module__ = __webpack_require__(493);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase_storage__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase_storage__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SettingsModule = /** @class */ (function () {
    function SettingsModule() {
    }
    SettingsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__settings__["a" /* Settings */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__settings__["a" /* Settings */]), __WEBPACK_IMPORTED_MODULE_3__app_pipes_module__["a" /* PipesModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__settings__["a" /* Settings */]]
        })
    ], SettingsModule);
    return SettingsModule;
}());

//# sourceMappingURL=settings.module.js.map

/***/ }),

/***/ 493:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_pipe__ = __webpack_require__(494);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(137);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__app_pipe__["a" /* AppPipe */]],
            imports: [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */], __WEBPACK_IMPORTED_MODULE_1__app_pipe__["a" /* AppPipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 494:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppPipe = /** @class */ (function () {
    function AppPipe() {
    }
    AppPipe.prototype.transform = function (value, args) {
        if (value) {
            var limit = args.length > 0 ? parseInt(args[0], 50) : 50;
            var trail = args.length > 1 ? args[1] : "...";
            return value.length > limit ? value.substring(0, limit) + trail : value;
        }
    };
    AppPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: "limitPipe"
        })
    ], AppPipe);
    return AppPipe;
}());

//# sourceMappingURL=app.pipe.js.map

/***/ }),

/***/ 781:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Settings; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase_app__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase_app__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Settings = /** @class */ (function () {
    function Settings(af, db, toastCtrl, loadingCtrl, navCtrl, platform, translate, events) {
        this.af = af;
        this.db = db;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.translate = translate;
        this.events = events;
        this.user = {};
        this.url = "assets/img/profile.jpg";
        this.options = [
            {
                language: "ENGLISH",
                value: "en"
            },
            {
                language: "FRENCH",
                value: "fr"
            },
            {
                language: "ARABIC",
                value: "ar"
            }
        ];
        this.file = {};
        this.storageRef = __WEBPACK_IMPORTED_MODULE_5_firebase_app__["storage"]();
        var value = localStorage.getItem("language");
        this.value = value != null ? value : "en";
        this.translate.setDefaultLang("en");
    }
    Settings.prototype.ngOnInit = function () {
        var _this = this;
        if (this.af.auth.currentUser) {
            this.db
                .object("/users/" + this.af.auth.currentUser.uid)
                .valueChanges()
                .subscribe(function (res) {
                _this.user = res;
                _this.user.image = res.image ? res.image : "";
                _this.url = res.image ? res.image : "assets/img/profile.jpg";
            });
        }
    };
    Settings.prototype.readUrl = function (event) {
        var _this = this;
        this.file = document.getElementById("file").files[0];
        var metadata = {
            contentType: "image/*"
        };
        var loader = this.loadingCtrl.create({
            content: "please wait.."
        });
        loader.present();
        this.storageRef
            .ref()
            .child("profile/" + this.file.name)
            .put(this.file, metadata)
            .then(function (res) {
            _this.user.image = res.downloadURL;
            _this.url = res.downloadURL;
            _this.db
                .object("users" + "/" + _this.af.auth.currentUser.uid + "/image")
                .set(res.downloadURL);
            loader.dismiss();
        })
            .catch(function (error) {
            loader.dismiss();
        });
    };
    Settings.prototype.changeLanguage = function () {
        localStorage.setItem("language", this.value);
        if (this.value == "fr") {
            this.platform.setDir("ltr", true);
            this.translate.use("fr");
        }
        else if (this.value == "ar") {
            this.platform.setDir("rtl", true);
            this.translate.use("ar");
        }
        else {
            this.platform.setDir("ltr", true);
            this.translate.use("en");
        }
    };
    Settings.prototype.onSubmit = function (user) {
        var _this = this;
        if (this.af.auth.currentUser) {
            this.db
                .object("/users/" + this.af.auth.currentUser.uid)
                .update({
                name: this.user.name,
                image: this.user.image,
                email: this.user.email,
                mobileNo: this.user.mobileNo
            })
                .then(function () {
                _this.createToaster("user information updated successfully", 3000);
                _this.events.publish("imageUrl", _this.user);
            });
        }
    };
    Settings.prototype.createToaster = function (message, duration) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration
        });
        toast.present();
    };
    Settings = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-settings",template:/*ion-inline-start:"C:\Users\1292344\Desktop\ionic-3-restaurantapp-master\src\pages\settings\settings.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{"Settings" | translate}}</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div class="login">\n        <ion-row>\n            <img class="img" [src]="url">\n            <br>\n        </ion-row>\n        <ion-row>\n            <input type="file" name="file" id="file" class="inputfile" (change)="readUrl($event)">\n            <label for="file" class="upload">\n                <ion-icon ios="ios-cloud-upload" md="md-cloud-upload"></ion-icon>\n            </label>\n        </ion-row>\n        <p class="name">{{user.name}}</p>\n    </div>\n    <ion-list>\n        <ion-item class="select-item">\n            <ion-label>{{"Language:" |translate }}</ion-label>\n            <ion-select [(ngModel)]="value" (ionChange)="changeLanguage()">\n                <ion-option *ngFor="let option of options" value="{{option.value}}"> {{option.language }}\n                </ion-option>\n            </ion-select>\n        </ion-item>\n    </ion-list>\n    <form (ngSubmit)="onSubmit(f)" #f="ngForm">\n        <ion-list>\n            <ion-item *ngIf="user.notification == true">\n                <ion-label primary>{{"Notifications" | translate}}</ion-label>\n                <ion-toggle primary checked="true" name="name" id="name" [(ngModel)]="user.notification"></ion-toggle>\n\n            </ion-item>\n            <ion-item *ngIf="user.notification == false">\n                <ion-label primary>{{"Notifications" | translate}}</ion-label>\n                <ion-toggle primary checked="false" name="name" id="name" [(ngModel)]="user.notification"></ion-toggle>\n\n            </ion-item>\n            <!--card no-->\n            <ion-item>\n                <ion-label>{{"User Name:" | translate}}</ion-label>\n                <ion-input type="text" name="name" id="name" placeholder="{{\'name\' | translate}}" [(ngModel)]="user.name" required></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label>{{"Email:" | translate}}</ion-label>\n                <ion-input type="email" name="email" id="email" placeholder="{{\'email\' | translate}}" [(ngModel)]="user.email" required></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label>{{"Mobile Number:" | translate}}</ion-label>\n                <ion-input type="text" name="mobileNo" id="mobileNo" placeholder="mobile number" [(ngModel)]="user.mobileNo" required></ion-input>\n            </ion-item>\n            <!--       <ion-item>\n                    <ion-label>Card Number:</ion-label>\n                    <ion-input type="text" name="card" id="card" placeholder="5172628928"\n                               [(ngModel)]="user.card"></ion-input>\n                  </ion-item> -->\n            <!--name on card-->\n            <!--       <ion-item>\n                    <ion-label>Name on card:</ion-label>\n                    <ion-input type="text" name="cardName" id="cardName" placeholder="Your card Name"\n                               [(ngModel)]="user.cardName"></ion-input>\n                  </ion-item> -->\n            <!--cvv-->\n            <!--       <ion-item>\n                    <ion-label>Cvv:</ion-label>\n                    <ion-input type="number" name="cvv" id="cvv" placeholder="234"\n                               [(ngModel)]="user.cvv"></ion-input>\n                  </ion-item> -->\n            <!--date-->\n            <!--       <ion-item>\n                    <ion-label>Date of expiry:</ion-label>\n                    <ion-input type="number" name="date" id="date" placeholder="25/06/2020"\n                               [(ngModel)]="user.date"></ion-input>\n                  </ion-item> -->\n        </ion-list>\n        <!--button-->\n        <button class="login-btn" ion-button type="submit" [disabled]="!f.valid">{{"Save" | translate}}</button>\n    </form>\n\n</ion-content>'/*ion-inline-end:"C:\Users\1292344\Desktop\ionic-3-restaurantapp-master\src\pages\settings\settings.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], Settings);
    return Settings;
}());

//# sourceMappingURL=settings.js.map

/***/ })

});
//# sourceMappingURL=11.js.map