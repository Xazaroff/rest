webpackJsonp([24],{

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressListPageModule", function() { return AddressListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__address_list__ = __webpack_require__(737);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_pipes_module__ = __webpack_require__(493);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AddressListPageModule = /** @class */ (function () {
    function AddressListPageModule() {
    }
    AddressListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__address_list__["a" /* AddressListPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__address_list__["a" /* AddressListPage */]), __WEBPACK_IMPORTED_MODULE_3__app_pipes_module__["a" /* PipesModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__address_list__["a" /* AddressListPage */]]
        })
    ], AddressListPageModule);
    return AddressListPageModule;
}());

//# sourceMappingURL=address-list.module.js.map

/***/ }),

/***/ 493:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_pipe__ = __webpack_require__(494);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(137);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__app_pipe__["a" /* AppPipe */]],
            imports: [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */], __WEBPACK_IMPORTED_MODULE_1__app_pipe__["a" /* AppPipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 494:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppPipe = /** @class */ (function () {
    function AppPipe() {
    }
    AppPipe.prototype.transform = function (value, args) {
        if (value) {
            var limit = args.length > 0 ? parseInt(args[0], 50) : 50;
            var trail = args.length > 1 ? args[1] : "...";
            return value.length > limit ? value.substring(0, limit) + trail : value;
        }
    };
    AppPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: "limitPipe"
        })
    ], AppPipe);
    return AppPipe;
}());

//# sourceMappingURL=app.pipe.js.map

/***/ }),

/***/ 737:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddressListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// declare let google: any;
var AddressListPage = /** @class */ (function () {
    function AddressListPage(af, db, navCtrl, navParams, alertCtrl) {
        var _this = this;
        this.af = af;
        this.db = db;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.address = {};
        this.addressList = [];
        this.orderDetails = {};
        this.pincodeMatched = false;
        this.loyaltyPercentage = 0;
        this.loyaltyPoints = 0;
        this.leftLoyaltyPoint = 0;
        this.checked = false;
        this.loyaltyArray = [];
        this.currency = JSON.parse(localStorage.getItem('currency'));
        this.orderDetails.grandTotal = this.navParams.get("grandTotal");
        this.payTotal = this.orderDetails.grandTotal;
        this.orderDetails.couponDiscount = this.navParams.get("couponDiscount");
        this.orderDetails.subTotal = this.navParams.get("subTotal");
        this.orderDetails.deductedPrice = this.navParams.get("deductedPrice");
        this.orderDetails.tax = this.navParams.get("totalVat");
        if (this.orderDetails.grandTotal == undefined) {
            this.navCtrl.push("CartPage");
        }
        if (this.af.auth.currentUser) {
            this.db
                .list("/users/" + this.af.auth.currentUser.uid + "/address")
                .snapshotChanges()
                .subscribe(function (res) {
                _this.addressList = [];
                res.forEach(function (item) {
                    var temp = item.payload.toJSON();
                    temp["$key"] = item.payload.key;
                    _this.addressList.push(temp);
                });
            });
            this.db
                .list("delivery-options")
                .valueChanges()
                .subscribe(function (res) {
                _this.pincodes = res;
                console.log("pincodes-" + JSON.stringify(res));
            });
            this.db
                .object("loyalitys")
                .valueChanges()
                .subscribe(function (loyalty) {
                var res = loyalty;
                if (res.enable) {
                    _this.loyaltyPercentage = res.loylityPercentage;
                    _this.loyaltyLimit = res.minLoyalityPointes;
                }
            });
            this.userRef = this.db.list("users/" + this.af.auth.currentUser.uid + "/loyaltyPoints");
            this.userRef.valueChanges().subscribe(function (res) {
                var points = res;
                _this.loyaltyArray = points;
                var _points = 0;
                for (var i = 0; i < _this.loyaltyArray.length; i++) {
                    _points = Number(Number(_points) + Number(_this.loyaltyArray[i].points));
                    _this.loyaltyPoints = _points;
                }
            });
        }
        this.orderDetails.cart = JSON.parse(localStorage.getItem("Cart"));
    }
    // Add Address
    AddressListPage.prototype.addAddress = function () {
        this.navCtrl.push("AddAddressPage", {
            id: 0
        });
    };
    //Selected Address
    AddressListPage.prototype.selectAddress = function (key, address) {
        this.pincodeMatched = false;
        this.orderDetails.shippingAddress = address;
        for (var i = 0; i < this.pincodes.length; i++) {
            if (this.pincodes[i].pincode == address.pincode) {
                this.pincodeMatched = true;
            }
        }
    };
    AddressListPage.prototype.checkOut = function () {
        this.orderDetails.usedLoyaltyPoints =
            this.checked == true ? this.loyaltyPoints : 0;
        this.orderDetails.appliedLoyaltyPoints = this.checked;
        this.orderDetails.orderView = false;
        if (this.orderDetails.shippingAddress && this.pincodeMatched) {
            this.navCtrl.push("CheckoutPage", {
                orderDetails: this.orderDetails
            });
        }
        else if (this.pincodeMatched == false) {
            this.showAlert("We can not deliver to your Area!");
        }
        else {
            this.showAlert("Select Your Address First!");
        }
    };
    AddressListPage.prototype.showAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: "Sorry!",
            subTitle: message,
            buttons: ["OK"]
        });
        alert.present();
    };
    AddressListPage.prototype.updateLoyality = function (event) {
        if (this.loyaltyPoints >= this.loyaltyLimit) {
            this.checked = event.value;
            if (event.value == true) {
                if (this.payTotal < this.loyaltyPoints) {
                    this.orderDetails.grandTotal = 0;
                    this.leftLoyaltyPoint = this.loyaltyPoints - this.payTotal;
                }
                else if (this.payTotal > this.loyaltyPoints) {
                    this.orderDetails.grandTotal = this.payTotal - this.loyaltyPoints;
                    this.leftLoyaltyPoint = 0;
                }
            }
            else {
                this.orderDetails.grandTotal = this.navParams.get("grandTotal");
            }
        }
    };
    AddressListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-address-list",template:/*ion-inline-start:"C:\Users\1292344\Desktop\ionic-3-restaurantapp-master\src\pages\address-list\address-list.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title class="title">{{"Delivery Options" | translate}}\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n    <!--amount calculation-->\n    <ion-row class="amount-block">\n        <ion-col col-6>\n            <p class="total"> {{"Order Total:" | translate}}\n                <span class="currency"> {{currency?.currencySymbol}} {{payTotal | number:\'.2-2\'}}\n                </span>\n            </p>\n        </ion-col>\n        <ion-col col-6>\n            <p class="total right">Loyality Points:\n                <span class="currency">{{ loyaltyPoints}}</span>\n            </p>\n        </ion-col>\n        <ion-item class="loyality-check">\n            <ion-label>Use Loyality Point</ion-label>\n            <ion-checkbox [disabled]="loyaltyPoints <loyaltyLimit" checked="checked" (ionChange)="updateLoyality($event)"></ion-checkbox>\n        </ion-item>\n        <p class="warning" *ngIf="loyaltyPoints <loyaltyLimit">you should have minimum {{loyaltyLimit}} points to use loyalty points</p>\n        <div *ngIf="checked == true">\n            <p class="total"> Amount to be paid:\n                <span class="currency"> {{currency?.currencySymbol}} {{orderDetails.grandTotal | number:\'.2-2\'}}</span>\n            </p>\n        </div>\n    </ion-row>\n    <!--pick up/delivery option-->\n\n    <div>\n        <ion-list radio-group class="size-list">\n            <!--New  Address-->\n            <ion-item class="add" (click)="addAddress()">{{"Add New Address" | translate}}\n                <ion-icon name="add"></ion-icon>\n            </ion-item>\n        </ion-list>\n        <!--saved address list-->\n        <ion-list radio-group class="address-radio">\n            <ion-item *ngFor="let address of addressList">\n                <ion-label>\n                    <p class="show-address">{{address.name}} </p>\n                    <p class="show-address">{{address.address}}</p>\n                    <p class="show-address">{{address.city}}</p>\n                    <p class="show-address">{{address.pincode}}</p>\n                </ion-label>\n                <ion-radio (ionSelect)="selectAddress(address.$key,address)"></ion-radio>\n            </ion-item>\n        </ion-list>\n        <button ion-button block class="continue-btn" (click)="checkOut()">{{"Continue" | translate}} &nbsp;\n            <ion-icon name="arrow-forward"></ion-icon>\n        </button>\n    </div>\n</ion-content>'/*ion-inline-end:"C:\Users\1292344\Desktop\ionic-3-restaurantapp-master\src\pages\address-list\address-list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], AddressListPage);
    return AddressListPage;
}());

//# sourceMappingURL=address-list.js.map

/***/ })

});
//# sourceMappingURL=24.js.map