webpackJsonp([28],{

/***/ 153:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 153;

/***/ }),

/***/ 196:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about-us/about-us.module": [
		464,
		7
	],
	"../pages/add-address/add-address.module": [
		466,
		25
	],
	"../pages/address-list/address-list.module": [
		465,
		24
	],
	"../pages/booking-history/booking-history.module": [
		467,
		27
	],
	"../pages/cart/cart.module": [
		468,
		23
	],
	"../pages/category/category.module": [
		469,
		22
	],
	"../pages/chat/chat.module": [
		470,
		0
	],
	"../pages/checkout/checkout.module": [
		491,
		6
	],
	"../pages/contact/contact.module": [
		472,
		9
	],
	"../pages/favourite/favourite.module": [
		474,
		21
	],
	"../pages/forgot-password/forgot-password.module": [
		471,
		2
	],
	"../pages/home/home.module": [
		475,
		20
	],
	"../pages/location/location.module": [
		473,
		3
	],
	"../pages/login/login.module": [
		476,
		1
	],
	"../pages/news-detail/news-detail.module": [
		477,
		19
	],
	"../pages/news/news.module": [
		478,
		18
	],
	"../pages/offer/offer.module": [
		479,
		17
	],
	"../pages/order-details/order-details.module": [
		482,
		5
	],
	"../pages/order-list/order-list.module": [
		480,
		16
	],
	"../pages/order-status/order-status.module": [
		481,
		15
	],
	"../pages/orders/orders.module": [
		483,
		14
	],
	"../pages/product-details/product-details.module": [
		485,
		13
	],
	"../pages/product-list/product-list.module": [
		484,
		4
	],
	"../pages/rating/rating.module": [
		486,
		8
	],
	"../pages/registration/registration.module": [
		488,
		12
	],
	"../pages/settings/settings.module": [
		489,
		11
	],
	"../pages/table-booking/table-booking.module": [
		490,
		26
	],
	"../pages/thankyou/thankyou.module": [
		487,
		10
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 196;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CartService = /** @class */ (function () {
    function CartService() {
        this.Cart = [];
        this.itemCart = {};
        this.itemInCart = [];
        this.Cart = JSON.parse(localStorage.getItem("Cart"));
    }
    CartService.prototype.OnsaveLS = function (item) {
        this.Cart = JSON.parse(localStorage.getItem("Cart"));
        var ExtotalPrice = 0;
        var totalPrice = 0;
        this.itemInCart = [];
        if (this.Cart == null) {
            for (var i = 0; i <= item.extraOptions.length - 1; i++) {
                ExtotalPrice = ExtotalPrice + Number(item.extraOptions[i].value);
            }
            if (item.price.specialPrice) {
                totalPrice = ExtotalPrice + Number(item.price.specialPrice);
            }
            else {
                totalPrice = ExtotalPrice + Number(item.price.value);
            }
            this.itemCart.itemTotalPrice = totalPrice * item.itemQunatity;
            this.itemCart.item = item;
            this.itemInCart.push(this.itemCart);
            localStorage.setItem("Cart", JSON.stringify(this.itemInCart));
        }
        else {
            for (var i = 0; i <= this.Cart.length - 1; i++) {
                if (this.Cart[i].item.itemId == item.itemId &&
                    this.Cart[i].item.price.pname == item.price.pname) {
                    this.Cart.splice(i, 1);
                }
            }
            for (var k = 0; k <= item.extraOptions.length - 1; k++) {
                ExtotalPrice = ExtotalPrice + Number(item.extraOptions[k].value);
            }
            if (item.price.specialPrice) {
                totalPrice = ExtotalPrice + Number(item.price.specialPrice);
            }
            else {
                totalPrice = ExtotalPrice + Number(item.price.value);
            }
            this.itemCart.itemTotalPrice = totalPrice * item.itemQunatity;
            this.itemCart.item = item;
            this.Cart.push(this.itemCart);
            localStorage.setItem("Cart", JSON.stringify(this.Cart));
        }
    };
    CartService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], CartService);
    return CartService;
}());

//# sourceMappingURL=cart.service.js.map

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(311);



Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__firebase_config__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_cart_service__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_http__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ngx_translate_http_loader__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_firebase_storage__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_firebase_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_firebase_storage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_date_picker__ = __webpack_require__(292);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_10__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, "./assets/i18n/", ".json");
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */]],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/about-us/about-us.module#AboutUsPageModule', name: 'AboutUsPage', segment: 'about-us', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/address-list/address-list.module#AddressListPageModule', name: 'AddressListPage', segment: 'address-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-address/add-address.module#AddAddressPageModule', name: 'AddAddressPage', segment: 'add-address', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/booking-history/booking-history.module#BookingHistoryPageModule', name: 'BookingHistoryPage', segment: 'booking-history', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cart/cart.module#CartPageModule', name: 'CartPage', segment: 'cart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/category/category.module#CategoryPageModule', name: 'CategoryPage', segment: 'category', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forgot-password/forgot-password.module#ForgotPasswordPageModule', name: 'ForgotPasswordPage', segment: 'forgot-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/location/location.module#LocationPageModule', name: 'LocationPage', segment: 'location', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/favourite/favourite.module#FavouritePageModule', name: 'FavouritePage', segment: 'favourite', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/news-detail/news-detail.module#NewsDetailPageModule', name: 'NewsDetailPage', segment: 'news-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/news/news.module#NewsPageModule', name: 'NewsPage', segment: 'news', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/offer/offer.module#OfferPageModule', name: 'OfferPage', segment: 'offer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/order-list/order-list.module#OrderListPageModule', name: 'OrderListPage', segment: 'order-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/order-status/order-status.module#OrderStatusPageModule', name: 'OrderStatusPage', segment: 'order-status', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/order-details/order-details.module#OrderDetailsPageModule', name: 'OrderDetailsPage', segment: 'order-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/orders/orders.module#OrdersPageModule', name: 'OrdersPage', segment: 'orders', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-list/product-list.module#ProductListPageModule', name: 'ProductListPage', segment: 'product-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-details/product-details.module#ProductDetailsPageModule', name: 'ProductDetailsPage', segment: 'product-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/rating/rating.module#RatingPageModule', name: 'RatingPage', segment: 'rating', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/thankyou/thankyou.module#ThankyouPageModule', name: 'ThankyouPage', segment: 'thankyou', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/registration/registration.module#RegistrationPageModule', name: 'RegistrationPage', segment: 'registration', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/settings/settings.module#SettingsModule', name: 'Settings', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/table-booking/table-booking.module#TableBookingPageModule', name: 'TableBookingPage', segment: 'table-booking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/checkout/checkout.module#CheckoutPageModule', name: 'CheckoutPage', segment: 'checkout', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_2_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_6__firebase_config__["a" /* firebaseConfig */]),
                __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: createTranslateLoader,
                        deps: [__WEBPACK_IMPORTED_MODULE_8__angular_common_http__["a" /* HttpClient */]]
                    }
                })
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__["a" /* BrowserModule */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */]],
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_7__pages_cart_service__["a" /* CartService */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_date_picker__["a" /* DatePicker */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__ = __webpack_require__(460);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__ = __webpack_require__(137);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MyApp = /** @class */ (function () {
    function MyApp(af, db, platform, statusbar, splashscreen, socialSharing, oneSignal, events, translateService) {
        this.af = af;
        this.db = db;
        this.platform = platform;
        this.statusbar = statusbar;
        this.splashscreen = splashscreen;
        this.socialSharing = socialSharing;
        this.oneSignal = oneSignal;
        this.events = events;
        this.translateService = translateService;
        this.Cart = [];
        this.imageUrl = "assets/img/profile.jpg";
        this.rootPage = "HomePage";
        this.initializeApp();
    }
    MyApp.prototype.ngOnInit = function () {
        var _this = this;
        this.uid = localStorage.getItem("uid");
        if (this.uid != null) {
            this.db
                .object("/users/" + this.uid)
                .valueChanges()
                .subscribe(function (res) {
                _this.name = res.name;
                _this.imageUrl =
                    res.image != "" && res.image != null
                        ? res.image
                        : "assets/img/profile.jpg";
            });
        }
        this.useTranslateService();
        this.getNewsCount();
        this.getOfferCount();
        this.listenEvents();
    };
    MyApp.prototype.getNewsCount = function () {
        var _this = this;
        this.db
            .list("/news")
            .valueChanges()
            .subscribe(function (res) {
            _this.noOfItemsInNews = res.length;
        });
    };
    MyApp.prototype.getOfferCount = function () {
        var _this = this;
        this.db
            .list("/menuItems", function (ref) { return ref.orderByChild("offer").equalTo(true); })
            .valueChanges()
            .subscribe(function (queriedItems) {
            _this.noOfItemsInOffer = queriedItems.length;
        });
    };
    MyApp.prototype.listenEvents = function () {
        var _this = this;
        this.events.subscribe("imageUrl", function (response) {
            _this.imageUrl =
                response.image != "" && response.image != null
                    ? response.image
                    : "assets/img/profile.jpg";
            _this.name = response.name;
        });
    };
    MyApp.prototype.useTranslateService = function () {
        var value = localStorage.getItem("language");
        var language = value != null ? value : "en";
        language == "ar"
            ? this.platform.setDir("rtl", true)
            : this.platform.setDir("ltr", true);
        this.translateService.use(language);
    };
    MyApp.prototype.openPage = function (page) {
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.db.object('/settings/currency').valueChanges().subscribe(function (res) {
            localStorage.setItem('currency', JSON.stringify(res));
        }, function (err) {
            localStorage.setItem('currency', JSON.stringify({ currencyName: 'USD', currencySymbol: '$' }));
        });
        if (this.platform.ready()) {
            this.platform.ready().then(function (res) {
                if (res == "cordova") {
                    _this.oneSignal.startInit("9740a50f-587f-4853-821f-58252d998399", "714618018341");
                    _this.oneSignal.getIds().then(function (response) {
                        if (_this.uid != null) {
                            _this.uid = localStorage.getItem("uid");
                            localStorage.setItem('playerId', response.userId);
                            _this.db.object("/users/" + _this.uid).update({
                                playerId: response.userId
                            });
                        }
                    });
                    _this.oneSignal.inFocusDisplaying(_this.oneSignal.OSInFocusDisplayOption.InAppAlert);
                    _this.oneSignal.handleNotificationReceived().subscribe(function () { });
                    _this.oneSignal.handleNotificationOpened().subscribe(function () { });
                    _this.oneSignal.endInit();
                }
            });
        }
    };
    MyApp.prototype.home = function () {
        this.nav.setRoot("HomePage");
    };
    MyApp.prototype.yourOrders = function () {
        this.nav.push("OrderListPage");
    };
    MyApp.prototype.addToCart = function () {
        this.nav.push("CartPage");
    };
    MyApp.prototype.catagory = function () {
        this.nav.push("CategoryPage");
    };
    MyApp.prototype.favourite = function () {
        this.nav.push("FavouritePage");
    };
    MyApp.prototype.offer = function () {
        this.nav.push("OfferPage");
    };
    MyApp.prototype.news = function () {
        this.nav.push("NewsPage");
    };
    MyApp.prototype.contact = function () {
        this.nav.push("ContactPage");
    };
    MyApp.prototype.aboutUs = function () {
        this.nav.push("AboutUsPage");
    };
    MyApp.prototype.settings = function () {
        this.nav.push("Settings");
    };
    MyApp.prototype.invite = function () {
        this.socialSharing.share("share Restaurant App with friends to get credits", null, null, "https://ionicfirebaseapp.com/#/");
    };
    MyApp.prototype.chat = function () {
        this.nav.push("ChatPage");
    };
    MyApp.prototype.tableBooking = function () {
        this.nav.push("TableBookingPage");
    };
    MyApp.prototype.bookingHistory = function () {
        this.nav.push("BookingHistoryPage");
    };
    MyApp.prototype.login = function () {
        this.nav.setRoot("LoginPage");
    };
    MyApp.prototype.logout = function () {
        this.af.auth.signOut();
        localStorage.removeItem("uid");
        localStorage.removeItem('playerId');
        this.imageUrl = "assets/img/profile.jpg";
        this.nav.setRoot("LoginPage");
    };
    MyApp.prototype.isLoggedin = function () {
        return localStorage.getItem("uid") != null;
    };
    MyApp.prototype.isCart = function () {
        this.Cart = JSON.parse(localStorage.getItem("Cart"));
        this.noOfItemsInCart = this.Cart != null ? this.Cart.length : null;
        return true;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\1292344\Desktop\ionic-3-restaurantapp-master\src\app\app.html"*/'<ion-menu persistent="true" class="menu" [content]="content" *ngIf="this.platform.dir()===\'ltr\'" side="left">\n  <ion-content class="sidebar-menu">\n    <ion-row class="img-name-row">\n      <ion-col>\n        <img class="user-img" src="{{imageUrl}}">\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col *ngIf="isLoggedin()">\n        <p class="user-name">{{name | uppercase}}</p>\n      </ion-col>\n    </ion-row>\n\n\n    <button menuClose class="menu-item" ion-item (click)="home()">\n      <ion-icon name="home" item-left></ion-icon>\n      {{\'Home\' | translate}}\n    </button>\n    <button menuClose class="menu-item" ion-item (click)="catagory()">\n      <ion-icon name="apps" item-left></ion-icon>\n      {{\'Category\'| translate}}\n    </button>\n    <button *ngIf="isCart()" menuClose class="menu-item" ion-item (click)="offer()">\n      <ion-icon name="pricetag" item-left></ion-icon>\n      {{\'Offers\'| translate}}\n      <ion-badge class="menu-badge">{{noOfItemsInOffer}}</ion-badge>\n    </button>\n    <button menuClose class="menu-item" ion-item (click)="addToCart()">\n      <ion-icon name="cart" item-left></ion-icon>\n      {{\'MyCart\'| translate}}\n      <ion-badge class="menu-badge">{{noOfItemsInCart}}</ion-badge>\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="yourOrders()">\n      <ion-icon name="timer" item-left></ion-icon>\n      {{\'Your Orders\'| translate}}\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="tableBooking()">\n      <ion-icon name="list-box" item-left></ion-icon>\n      {{\'Book a Table\' | translate}}\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="bookingHistory()">\n      <ion-icon name="options" item-left></ion-icon>\n      <!-- <ion-badge class="menu-badge">4</ion-badge> -->\n      {{\'Booking History\' | translate}}\n    </button>\n    <button menuClose class="menu-item" ion-item (click)="news()">\n      <ion-icon name="paper" item-left></ion-icon>\n      {{\'News\'| translate}}\n      <ion-badge class="menu-badge">{{noOfItemsInNews}}</ion-badge>\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="favourite()">\n      <ion-icon name="heart" item-left></ion-icon>\n      {{\'Favourite\'| translate}}\n      <!-- <ion-badge class="menu-badge">{{noOfItemsInFevrt}}</ion-badge> -->\n    </button>\n    <button menuClose class="menu-item" ion-item (click)="contact()">\n      <ion-icon name="call" item-left></ion-icon>\n      {{\'Contact\'| translate}}\n    </button>\n    <button menuClose class="menu-item" ion-item (click)="aboutUs()">\n      <ion-icon name="contacts" item-left></ion-icon>\n      {{\'About Us\'| translate}}\n    </button>\n    <button menuClose class="menu-item" ion-item *ngIf="isLoggedin()" (click)="invite()">\n      <ion-icon name="contacts" item-left></ion-icon>\n      {{\'Invite Frieds\' | translate}}\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="chat()">\n      <ion-icon name="chatbubbles" item-left></ion-icon>\n      {{\'Chat\' | translate}}\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="settings()">\n      <ion-icon name="settings" item-left></ion-icon>\n      {{\'Settings\'| translate}}\n    </button>\n    <button *ngIf="!isLoggedin()" menuClose class="menu-item" ion-item (click)="login()">\n      <ion-icon name="log-in" item-left></ion-icon>\n      {{\'Login\'| translate}}\n    </button>\n    <button *ngIf="isLoggedin()" ion-item (click)="logout()" menuClose class="menu-item">\n      <ion-icon name="log-out" item-left></ion-icon>\n      {{\'Logout\'| translate}}\n    </button>\n  </ion-content>\n</ion-menu>\n\n<ion-menu persistent="true" class="menu" [content]="content" *ngIf="this.platform.dir()===\'rtl\'" side="right">\n  <ion-content class="sidebar-menu">\n    <ion-row class="img-name-row">\n      <ion-col>\n        <img class="user-img" src="{{imageUrl}}">\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col *ngIf="isLoggedin()">\n        <p class="user-name">{{name}}</p>\n      </ion-col>\n    </ion-row>\n\n\n    <button menuClose class="menu-item" ion-item (click)="home()">\n      <ion-icon name="home" item-left></ion-icon>\n      {{\'Home\' | translate}}\n    </button>\n    <button menuClose class="menu-item" ion-item (click)="catagory()">\n      <ion-icon name="apps" item-left></ion-icon>\n      {{\'Category\'| translate}}\n    </button>\n    <button *ngIf="isCart()" menuClose class="menu-item" ion-item (click)="offer()">\n      <ion-icon name="pricetag" item-left></ion-icon>\n      {{\'Offers\'| translate}}\n      <ion-badge class="menu-badge">{{noOfItemsInOffer}}</ion-badge>\n    </button>\n    <button menuClose class="menu-item" ion-item (click)="addToCart()">\n      <ion-icon name="cart" item-left></ion-icon>\n      {{\'MyCart\'| translate}}\n      <ion-badge class="menu-badge">{{noOfItemsInCart}}</ion-badge>\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="yourOrders()">\n      <ion-icon name="timer" item-left></ion-icon>\n      {{\'Your Orders\'| translate}}\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="tableBooking()">\n      <ion-icon name="list-box" item-left></ion-icon>\n      {{\'Book a Table\' | translate}}\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="bookingHistory()">\n      <ion-icon name="options" item-left></ion-icon>\n      <!-- <ion-badge class="menu-badge">4</ion-badge> -->\n      {{\'Booking History\' | translate}}\n    </button>\n    <button menuClose class="menu-item" ion-item (click)="news()">\n      <ion-icon name="paper" item-left></ion-icon>\n      {{\'News\'| translate}}\n      <ion-badge class="menu-badge">{{noOfItemsInNews}}</ion-badge>\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="favourite()">\n      <ion-icon name="heart" item-left></ion-icon>\n      {{\'Favourite\'| translate}}\n      <!-- <ion-badge class="menu-badge">{{noOfItemsInFevrt}}</ion-badge> -->\n    </button>\n    <button menuClose class="menu-item" ion-item (click)="contact()">\n      <ion-icon name="call" item-left></ion-icon>\n      {{\'Contact\'| translate}}\n    </button>\n    <button menuClose class="menu-item" ion-item (click)="aboutUs()">\n      <ion-icon name="contacts" item-left></ion-icon>\n      {{\'About Us\'| translate}}\n    </button>\n    <button menuClose class="menu-item" ion-item *ngIf="isLoggedin()" (click)="invite()">\n      <ion-icon name="contacts" item-left></ion-icon>\n      {{\'Invite Frieds\' | translate}}\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="chat()">\n      <ion-icon name="chatbubbles" item-left></ion-icon>\n      {{\'Chat\' | translate}}\n    </button>\n    <button *ngIf="isLoggedin()" menuClose class="menu-item" ion-item (click)="settings()">\n      <ion-icon name="settings" item-left></ion-icon>\n      {{\'Settings\'| translate}}\n    </button>\n    <button *ngIf="!isLoggedin()" menuClose class="menu-item" ion-item (click)="login()">\n      <ion-icon name="log-in" item-left></ion-icon>\n      {{\'Login\'| translate}}\n    </button>\n    <button *ngIf="isLoggedin()" ion-item (click)="logout()" menuClose class="menu-item">\n      <ion-icon name="log-out" item-left></ion-icon>\n      {{\'Logout\'| translate}}\n    </button>\n  </ion-content>\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\1292344\Desktop\ionic-3-restaurantapp-master\src\app\app.html"*/,
            selector: "MyApp",
            providers: [__WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__["a" /* OneSignal */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__["a" /* SocialSharing */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__["c" /* TranslateService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return firebaseConfig; });
//config default
var firebaseConfig = {
    apiKey: "AIzaSyCmGkqS8t4j9Dj1iInF7iqQGbzL0y43_6k",
    authDomain: "mangal-8adab.firebaseapp.com",
    databaseURL: "https://mangal-8adab.firebaseio.com",
    projectId: "mangal-8adab",
    storageBucket: "mangal-8adab.appspot.com",
    messagingSenderId: "880430029413",
    appId: "1:880430029413:web:dfca110fc43da2f7"
    /*apiKey: "AIzaSyA9tJ1p8iDcYy6tLoOisnr4zXFbS8aHEag",
    authDomain: "restaurant-1440e.firebaseapp.com",
    databaseURL: "https://restaurant-1440e.firebaseio.com",
    projectId: "restaurant-1440e",
    storageBucket: "restaurant-1440e.appspot.com",
    messagingSenderId: "314198464457"*/
};
// Sandip's firebase configuration
// export const firebaseConfig = {
//     apiKey: "AIzaSyAzusy10a0pSk-huxJO-DUtK0RoGgqJcpM",
//     authDomain: "restaurantweb-ionic-app.firebaseapp.com",
//     databaseURL: "https://restaurantweb-ionic-app.firebaseio.com",
//     projectId: "retsaurantweb-ionic-app",
//     storageBucket: "restaurantweb-ionic-app.appspot.com",
//     messagingSenderId: "120142074708"
//   };
//# sourceMappingURL=firebase.config.js.map

/***/ })

},[294]);
//# sourceMappingURL=main.js.map