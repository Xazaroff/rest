webpackJsonp([26],{

/***/ 490:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableBookingPageModule", function() { return TableBookingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__table_booking__ = __webpack_require__(782);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TableBookingPageModule = /** @class */ (function () {
    function TableBookingPageModule() {
    }
    TableBookingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__table_booking__["a" /* TableBookingPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__table_booking__["a" /* TableBookingPage */])]
        })
    ], TableBookingPageModule);
    return TableBookingPageModule;
}());

//# sourceMappingURL=table-booking.module.js.map

/***/ }),

/***/ 782:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableBookingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(138);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TableBookingPage = /** @class */ (function () {
    function TableBookingPage(navCtrl, datePicker, dbService, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.datePicker = datePicker;
        this.dbService = dbService;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.uid = localStorage.getItem("uid");
    }
    TableBookingPage.prototype.onClickBookTable = function () {
        if (this.time && this.date && this.person != undefined) {
            this.bookTable = {
                userID: this.uid,
                status: "Pending",
                time: this.time,
                date: this.date,
                person: this.person
            };
            this.presentConfirm();
        }
        else {
            this.presentToast("All filed Required to book table.");
        }
    };
    TableBookingPage.prototype.presentConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Confirm Booking",
            message: "Sure? Do you want to book table?",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                        _this.presentToast("Not Intrested?. Going back...");
                        _this.navCtrl.pop();
                    }
                },
                {
                    text: "Book",
                    handler: function () {
                        _this.dbService
                            .list("table-bookings")
                            .push(_this.bookTable)
                            .then(function (res) {
                            _this.presentToast("Table has been Booked succesfully!");
                            _this.navCtrl.pop();
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    TableBookingPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: "bottom"
        });
        toast.present();
    };
    TableBookingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-table-booking",template:/*ion-inline-start:"C:\Users\1292344\Desktop\ionic-3-restaurantapp-master\src\pages\table-booking\table-booking.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title class="title">Book a Table</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content class="content-wrap">\n  <div class="side-headline">\n    <p class="heading"> Select Date and Time to Book Table!</p>\n  </div>\n  <div class="order">\n    <ion-label class="form">&nbsp;&nbsp;Select Date:</ion-label>\n    <div class="select-box">\n      <ion-item class="data-holder">\n        <ion-datetime displayFormat="DD-MM-YYYY" [(ngModel)]="date"></ion-datetime>\n        <ion-label>\n          <ion-icon name="calendar" class="icons"></ion-icon>\n        </ion-label>\n      </ion-item>\n    </div>\n  </div>\n  <!--order-->\n  <div class="order">\n    <ion-label class="form">&nbsp;&nbsp;Select Time:</ion-label>\n    <div class="select-box">\n      <ion-item class="data-holder">\n        <ion-datetime displayFormat="hh-mm" [(ngModel)]="time"></ion-datetime>\n        <ion-label>\n          <ion-icon name="time" class="icons"></ion-icon>\n        </ion-label>\n      </ion-item>\n    </div>\n  </div>\n  <!--order-->\n  <div class="order">\n    <ion-label class="form">&nbsp;&nbsp;Select No. of Guests:</ion-label>\n    <div class="select-box">\n      <ion-item class="data-holder">\n        <!-- <ion-item> -->\n        <ion-label>No. of Guests&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</ion-label>\n        <ion-select [(ngModel)]="person">\n          <ion-option value="1">1 person</ion-option>\n          <ion-option value="2">2 persons</ion-option>\n          <ion-option value="3">3 persons</ion-option>\n          <ion-option value="4">4 persons</ion-option>\n          <ion-option value="5">5 persons</ion-option>\n          <ion-option value="5+">5+ persons</ion-option>\n        </ion-select>\n        <!-- </ion-item> -->\n      </ion-item>\n    </div>\n    <!--select-box-->\n  </div>\n  <!--order-->\n  <div class="btn-wrap">\n    <button ion-button full type="button" class="book-button" (click)="onClickBookTable()">Reserve a Table</button>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\1292344\Desktop\ionic-3-restaurantapp-master\src\pages\table-booking\table-booking.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */]])
    ], TableBookingPage);
    return TableBookingPage;
}());

//# sourceMappingURL=table-booking.js.map

/***/ })

});
//# sourceMappingURL=26.js.map